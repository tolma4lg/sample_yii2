<?php

namespace common\models\tables;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $posts_parsed
 * @property integer $category
 *
 * @property GroupsCategories $category0
 * @property GroupsInfo[] $groupsInfos
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'posts_parsed'], 'required'],
            [['id', 'type', 'posts_parsed', 'category'], 'integer'],
            [['id'], 'unique'],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => GroupsCategories::className(), 'targetAttribute' => ['category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'posts_parsed' => 'Posts Parsed',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(GroupsCategories::className(), ['id' => 'category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupsInfos()
    {
        return $this->hasMany(GroupsInfo::className(), ['group_id' => 'id']);
    }
}