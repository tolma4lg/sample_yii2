<?php

namespace common\models\tables;

use Yii;

/**
 * This is the model class for table "groups_info".
 *
 * @property integer $group_id
 * @property string $photo
 * @property string $name
 * @property integer $wall_count
 * @property string $descr
 * @property integer $members
 *
 * @property Groups $group
 */
class GroupsInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'photo', 'wall_count', 'members'], 'required'],
            [['group_id', 'wall_count', 'members'], 'integer'],
            [['photo', 'name', 'descr'], 'string'],
            [['group_id'], 'unique'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'photo' => 'Photo',
            'name' => 'Name',
            'wall_count' => 'Wall Count',
            'descr' => 'Descr',
            'members' => 'Members',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    public function getPermalink()
    {
        return 'https://vk.com/club' . $this->group_id;
    }
}