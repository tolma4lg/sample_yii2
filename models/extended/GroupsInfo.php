<?php

namespace models\extended;

use Yii;

class GroupsInfo extends \common\models\tables\GroupsInfo
{
    public static function setForGroup($params)
    {
        if (isset($params['group_id'])) {
            if ($group = Groups::findOne(['id' => $params['group_id']])) {

                $info = $group->info;

                foreach ($params as $param_name => $param_value) {
                    $info->{$param_name} = $param_value;
                }

                try {
                    if (!$info->validate() || !$info->save()) {
                        throw new \Exception('Unable to save');
                    }
                } catch (\Exception $e) {
                    var_dump($e->getMessage(),$info->getErrors());
                }
            }
        }
    }

    public function beforeValidate()
    {
        // $this->photo = preg_replace('/[^(\x20-\x7F)]*/','', $this->photo);
        // $this->name  = preg_replace('/[^(\x20-\x7F)]*/','', $this->name);
        // $this->descr = preg_replace('/[^(\x20-\x7F)]*/','', $this->descr);

        return parent::beforeValidate();
    }

    public function getPermalink()
    {
        return 'https://vk.com/club' . $this->group_id;
    }
}