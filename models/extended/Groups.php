<?php

namespace common\models\extended;

use Yii;

class Groups extends \common\models\tables\Groups
{
    const TYPE_GROUP = 1;
    const TYPE_PAGE  = 2;
    const TYPE_EVENT = 3;

    public function setType($name)
    {
        if (defined('static::TYPE_' . strtoupper($name))) {
            $this->type = constant('static::TYPE_' . strtoupper($name));
        } else {
            throw new \Exception('Unable to determine group type');
        }
    }

    public function getPermalink()
    {
        switch ($this->type) {
            case static::TYPE_GROUP:
                $type = 'club';
                break;
            case static::TYPE_PAGE:
                $type = 'public';
                break;
            case static::TYPE_EVENT:
                $type = 'event';
                break;
            default:
                return '#';
        }

        return 'https://vk.com/' . $type . $this->id;
    }

    public function getParsedPostsCount()
    {
        return 0;
    }

    public function getParsedPostsUseful()
    {
        return 0;
    }

    public function getLastPost()
    {
        $max = Posts::find()
            ->select(['if(max(group_post_id), max(group_post_id), 0)'])
            ->where(['group_id' => $this->id])
            ->scalar();

        return empty($max) || $max < $this->last_post
            ? $this->last_post
            : $max;
    }

    public function getGroupsInfos()
    {
        return $this->hasOne(GroupsInfo::className(), ['group_id' => 'id']);
    }

    public function getInfo()
    {
        if (!empty($this->groupsInfos)) {
            return $this->groupsInfos;
        } else {
            $info = new GroupSInfo();
            $info->group_id = $this->id;
            return $info;
        }
    }

    public function handlePost($item)
    {
        try {
            /* Yii::$app->db->open(); */

            $post = new Posts([
                'item'     => $item,
                'group'    => $this,
                'group_id' => $this->id
            ]);

            $execution_result =  $post->parse() && $post->check() && $post->save();

            /* Yii::$app->db->close(); */

            return $execution_result;

        } catch (\Exception $e) {
            echo 'Error occured in ' . __FILE__ . ':' . __LINE__ . PHP_EOL . $e->getMessage() . '//' . $e->getFile() . '//' . $e->getLine() . PHP_EOL . PHP_EOL;
            /* echo $e; */
            /*             var_dump($e->getTrace());
                        echo PHP_EOL;
                        echo PHP_EOL;
                        echo PHP_EOL; */
        }
    }

    public function updateLastPost($count)
    {
        if ($count > $this->last_post) {
            $this->last_post = $count;
            return $this->save();
        }

        return false;
    }
}