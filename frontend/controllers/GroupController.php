<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;

use common\models\extended\Groups;
use common\models\extended\GroupsInfo;

class GroupController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public $layout = 'default';

    public function actionWithoutInfo()
    {
        $query = Groups::find()
            ->join('LEFT JOIN', 'groups_info', new \yii\db\Expression('groups_info.group_id = groups.id'))
            ->where('groups_info.name = ""')
            ->orderBy('id');

        $pagination = new Pagination([
            'totalCount' => $query->count()
        ]);

        $pagination->setPageSize(24);

        $groups = $query
            ->with('groupsInfos')
            ->offset($pagination->offset)
            ->limit(24)
            ->all();

        return $this->render('index', [
            'groups'     => $groups,
            'pagination' => $pagination
        ]);
    }

    /*     public function actionIndex()
        {
            $query = \common\models\mongo\Groups::find();

            $pagination = new Pagination([
                'totalCount' => $query->count(),
                'pageSize'   => 24,
            ]);

            return $this->render('/site/groups', [
                'pagination' => $pagination,
                'groups'     => $query
                    ->limit($pagination->limit)
                    ->offset($pagination->offset)
                    ->all(),
            ]);
        } */

    public function actionIndex()
    {
        $query = Groups::find()
            ->orderBy('id');

        $pagination = new Pagination([
            'totalCount' => $query->count()
        ]);

        $pagination->setPageSize(24);

        $groups = $query
            ->with('groupsInfos')
            ->offset($pagination->offset)
            ->limit(24)
            ->all();

        return $this->render('index', [
            'groups'     => $groups,
            'pagination' => $pagination
        ]);
    }

    public function actionShow($id)
    {
        if ($group = Groups::findOne($id)) {
            return $this->render('show', [
                'group' => $group,
            ]);
        } else {
            return $this->redirect(['/group']);
        }
    }

    public function actionLol()
    {
        return $this->render('lol', [
            'groups'     => Groups::find()
        ]);
    }

    public function actionAjaxUpdate()
    {
        if (!empty(Yii::$app->request->post('response'))) {
            foreach (Yii::$app->request->post('response') as $group_info_response) {
                $group_info = GroupsInfo::setForGroup($group_info_response);
            }
        }
    }
}